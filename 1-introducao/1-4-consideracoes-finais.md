# 1.4 Considerações Finais

Neste capítulo apresentamos uma descrição geral e motivacional sobre o uso do Desenvolvimento Dirigido por Testes (TDD) na concepção de uma aplicação simples. Como observado, TDD tem tudo a ver com testes. Pensar em como validar um artefato em desenvolvimento faz com que o desenvolvedor tenha certeza do que o respectivo artefato deve fazer, ou seja, como ele deve se comportar ao receber entradas para serem processadas.

Outro aspecto importante do TDD é que, ao se pensar no teste para validar determinada funcionalidade, é de fundamental importância entender o que essa funcionalidade precisa de informação para prestar o serviço para o qual foi especificada e qual deve ser o resultado de sua execução uma vez que receba determinada entrada. Em outras palavras, TDD demanda uma especificação do que deve ser desenvolvido e apenas com bons testes pode-se assegurar que ao final do desenvolvimento com TDD têm-se a aplicação que se deseja.&#x20;

Nesse sentido, o capítulo a seguir apresenta alguns conceitos básicos sobre teste de software para auxiliar o desenvolvedor a elaborar um conjunto de teste consistente, e com base em critérios de teste, para iniciar o desenvolvimento de uma aplicação.
