# 2.5 Considerações Finais

Neste capítulo abordamos a terminologia e conceitos básicos sobre teste de software. Apresentamos uma breve descrição das principais técnicas de teste, com ênfase para a técnica de teste funcional ou caixa-preta pois é ela a mais utilizada no contexto do TDD.

Apresentamos também dois dos critérios de teste populares da técnica funcional: particionamento em classe de equivalência e análise do valor limite.

Em geral, ambos critérios são utilizados intuitivamente pelos testadores e foram formalizados aqui.

Foram apresentados exemplos ilustrando a aplicação dos critérios visando facilitar o desenvolvimento de novos casos de teste para outras especificações, contribuindo com a aplicação do TDD.
