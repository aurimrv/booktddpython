# 3.1 Configuração do Ambiente

Considerando a aplicação que será desenvolvida, ela fará uso do framework Django e para realizarmos o TDD, também precisaremos do Selenium. Segue abaixo a instrução para a instalação de ambos.Após a instalação das ferramentas básicas, conforme apresentado na [Seção 1.2](../1-introducao/1-2-configuracao-inicial-do-ambiente.md), a seguir são apresentadas as demais configurações para dar início ao desenvolvimento da aplicação exemplo com TDD.&#x20;

O primeiro passo é verificarmos as versões das ferramentas já instaladas. Os comandos a seguir permitem fazer tal verificação.

```bash
$ pyenv --version
pyenv 2.4.3
    
$ pyenv virtualenvs
  3.8.10/envs/superlists (created from /home/tdd/.pyenv/versions/3.8.10)
  superlists (created from /home/tdd/.pyenv/versions/3.8.10)

$ pyenv virtualenv-delete superlists
pyenv-virtualenv: remove /home/tdd/.pyenv/versions/3.8.10/envs/superlists? (y/N) y

$ git --version
git version 2.34.1
```

Caso tenha perdido os passos dessa instalação, retorne à [Seção 1.2](../1-introducao/1-2-configuracao-inicial-do-ambiente.md) e proceda a configuração inicial do ambiente.

Com essas ferramentas instaladas e as variáveis de ambiente definidas, é possível agora iniciarmos a instalação das ferramentas do Python dentro do ambiente virtual criado anteriormente.&#x20;

#### Configurando o Ambiente Virtual

O primeiro passo para dar início ao desenvolvimento da nossa aplicação é criação e configuração do ambiente virtual. Para isso, vamos executar os comandos abaixo:

```bash
$ mkdir superlists
$ cd $HOME/superlists
$ pyenv virtualenv 3.10.12 superlists
$ pyenv activate superlists
$ (superlists) tdd@mlp:~/superlists$
```

Ao final da execução do comando `pyenv activate superlists`, observa-se uma pequena mudança no _prompt_ de comando, ao invés de aparecer apenas um sinal de `$`, o _prompt_ passa a ser precedido por `(superlists)$` , indicando que você está conectado ao ambiente virtual.&#x20;

Os comandos a seguir mostram como desativar e ativar o `virtualenv`. Ou seja, estando no ambiente virtual, basta usar o comando `deactivate` que você deixa o ambiente. Se necessitar retornar, basta executar o comando `workon superlists`.

```bash
(superlists) $ pyenv deactivate 

tdd@mlp:~/superlists$
```

#### Instalação do Django e Selenium dentro do Ambiente Virtual

O [django](https://www.djangoproject.com/) é um framework para construção de sistemas Web em Python e o [Selenium](https://selenium-python.readthedocs.io/) é uma ferramenta de captura e reprodução que permite a execução de testes automatizados via navegador Web. Para instalar ambas as ferramentas e utilizá-las em nossos projetos, primeiro vamos criar o arquivo denominado `requirements.txt` e, dentro desse arquivo incluírmos o seguinte conteúdo:

```
django==5.0.6
selenium==4.22.0
```

A vantagens de usarmos o arquivo requirements.txt é que deixamos documentadas as versões dos plugins e ferramenas que estamos utilizando. Nesse caso, estamos instalando o Django e Selenium nas versões, 5.0.6 e 4.22.0, respectivamente. Para proceder com a instalação, estando com o ambiente virtual ativado, basta executar o comando `pip`, conforme abaixo:

```bash
tdd@mlp:~/superlists$ pyenv activate superlists
(superlists) auri@av:~/superlists$ pip install -r requirements.txt
Collecting django==5.0.6
  Using cached Django-5.0.6-py3-none-any.whl (8.2 MB)
Collecting selenium==4.22.0
  Downloading selenium-4.22.0-py3-none-any.whl (9.4 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 9.4/9.4 MB 17.4 MB/s eta 0:00:00
Collecting sqlparse>=0.3.1
  Using cached sqlparse-0.5.0-py3-none-any.whl (43 kB)
Collecting asgiref<4,>=3.7.0
  Using cached asgiref-3.8.1-py3-none-any.whl (23 kB)
Collecting trio-websocket~=0.9
  Downloading trio_websocket-0.11.1-py3-none-any.whl (17 kB)
Collecting urllib3[socks]<3,>=1.26
  Downloading urllib3-2.2.2-py3-none-any.whl (121 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 121.4/121.4 kB 5.0 MB/s eta 0:00:00
Collecting certifi>=2021.10.8
  Downloading certifi-2024.6.2-py3-none-any.whl (164 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 164.4/164.4 kB 4.8 MB/s eta 0:00:00
Collecting typing_extensions>=4.9.0
  Using cached typing_extensions-4.12.2-py3-none-any.whl (37 kB)
Collecting websocket-client>=1.8.0
  Downloading websocket_client-1.8.0-py3-none-any.whl (58 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 58.8/58.8 kB 2.9 MB/s eta 0:00:00
Collecting trio~=0.17
  Downloading trio-0.25.1-py3-none-any.whl (467 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 467.7/467.7 kB 3.1 MB/s eta 0:00:00
Collecting sortedcontainers
  Downloading sortedcontainers-2.4.0-py2.py3-none-any.whl (29 kB)
Collecting sniffio>=1.3.0
  Downloading sniffio-1.3.1-py3-none-any.whl (10 kB)
Collecting attrs>=23.2.0
  Downloading attrs-23.2.0-py3-none-any.whl (60 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 60.8/60.8 kB 6.8 MB/s eta 0:00:00
Collecting idna
  Downloading idna-3.7-py3-none-any.whl (66 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 66.8/66.8 kB 6.6 MB/s eta 0:00:00
Collecting exceptiongroup
  Downloading exceptiongroup-1.2.1-py3-none-any.whl (16 kB)
Collecting outcome
  Downloading outcome-1.3.0.post0-py2.py3-none-any.whl (10 kB)
Collecting wsproto>=0.14
  Downloading wsproto-1.2.0-py3-none-any.whl (24 kB)
Collecting pysocks!=1.5.7,<2.0,>=1.5.6
  Downloading PySocks-1.7.1-py3-none-any.whl (16 kB)
Collecting h11<1,>=0.9.0
  Downloading h11-0.14.0-py3-none-any.whl (58 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 58.3/58.3 kB 7.3 MB/s eta 0:00:00
Installing collected packages: sortedcontainers, websocket-client, urllib3, typing_extensions, sqlparse, sniffio, pysocks, idna, h11, exceptiongroup, certifi, attrs, wsproto, outcome, asgiref, trio, django, trio-websocket, selenium
Successfully installed asgiref-3.8.1 attrs-23.2.0 certifi-2024.6.2 django-5.0.6 exceptiongroup-1.2.1 h11-0.14.0 idna-3.7 outcome-1.3.0.post0 pysocks-1.7.1 selenium-4.22.0 sniffio-1.3.1 sortedcontainers-2.4.0 sqlparse-0.5.0 trio-0.25.1 trio-websocket-0.11.1 typing_extensions-4.12.2 urllib3-2.2.2 websocket-client-1.8.0 wsproto-1.2.0

[notice] A new release of pip is available: 23.0.1 -> 24.1
[notice] To update, run: python -m pip install --upgrade pip
(superlists) tdd@mlp:~$ 
```

O Selenium permite a execução dos testes em vários navegadores mas, para cada um deles, é necessário baixar um driver correspondente. No nosso exemplo, faremos o download dos drivers para Firefox, denominado de [Gecko Driver](https://github.com/mozilla/geckodriver/releases), e Google Chrome, denominado Chrome Driver, conforme descrito a seguir. Você deve baixar o driver na versão compatível com a de seu navegador web. No caso abaixo, utilizamos o [Gecko Driver 0.34.0](https://github.com/mozilla/geckodriver/releases/download/v0.34.0/geckodriver-v0.34.0-linux64.tar.gz) e o [ChromeDriver 126.0.6478.63](https://googlechromelabs.github.io/chrome-for-testing/), ambos para sistemas Linux 64 bits.

Primeiro é necessário fazer o download dos arquivos compactados com os drivers acima e, em seguida, a instalação de ambos os drivers é feita simplesmente descompactando o conteúdo dos arquivos dentro da pasta `/home/mlptdd/.pyenv/versions/3.10.2/envs/superlists/bin`, conforme apresentado a seguir:

```bash
$ cd $HOME/.pyenv/bin
$ wget https://github.com/mozilla/geckodriver/releases/download/v0.34.0/geckodriver-v0.34.0-linux64.tar.gz
$ tar zxvf geckodriver-v0.34.0-linux64.tar.gz

$ wget https://storage.googleapis.com/chrome-for-testing-public/126.0.6478.63/linux64/chromedriver-linux64.zip
$ unzip chromedriver-linux64.zip 
$ mv chromedriver-linux64/chromedriver .

$ rm -rf geckodriver-v0.34.0-linux64.tar.gz chromedriver-linux64*
```

Feita as instalações, é possível conferir as versões dos drivers com os comandos abaixo:

```bash
$ cd $HOME/.pyenv/bin
$ ./geckodriver --version
geckodriver 0.34.0 (c44f0d09630a 2024-01-02 15:36 +0000)

The source code of this program is available from
testing/geckodriver in https://hg.mozilla.org/mozilla-central.

This program is subject to the terms of the Mozilla Public License 2.0.
You can obtain a copy of the license at https://mozilla.org/MPL/2.0/.
```

```bash
./chromedriver --version
ChromeDriver 126.0.6478.63 (df799988fdc9c52db48650316d53800b1e9aa69e-refs/branch-heads/6478_56@{#5})
```

Pronto. Ao final de todos esses passos estamos prontos para dar início ao desenvolvimento de nossa aplicação com base no TDD. Vamos lá!?
