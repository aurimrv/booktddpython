# Table of contents

* [Programação e Desenvolvimento Dirigidor por Testes em Python](README.md)
* [Autores e Agradecimentos](autores-e-agradecimentos.md)
* [Uso do Livro](uso-do-livro.md)
* [Contribua com o Livro](contribua-com-o-livro.md)
* [Licença](licenca.md)
* [Organização do Livro](organizacao-do-livro.md)

## 1 Introdução

* [1.1 Considerações Iniciais](1-introducao/1-1-consideracoes-iniciais.md)
* [1.2 Configuração Inicial do Ambiente](1-introducao/1-2-configuracao-inicial-do-ambiente.md)
* [1.3 TDD Básico](1-introducao/1-3-tdd-basico/README.md)
  * [1.3.1 Exemplo Simples](1-introducao/1-3-tdd-basico/1.3.1-exemplo-simples.md)
* [1.4 Considerações Finais](1-introducao/1-4-consideracoes-finais.md)

## 2 TESTE DE SOFTWARE

* [2.1 Considerações Iniciais](2-teste-de-software/2-1-consideracoes-iniciais.md)
* [2.2 Terminologia e Conceitos Básicos](2-teste-de-software/2-2-terminologia-e-conceitos-basicos.md)
* [2.3 Fases de Teste](2-teste-de-software/2-3-fases-de-teste.md)
* [2.4 Técnicas e Critérios de Teste](2-teste-de-software/2-4-tecnicas-e-criterios-de-teste.md)
* [2.5 Considerações Finais](2-teste-de-software/2-5-consideracoes-finais.md)

## 3 Desenvolvimento Dirigido por Teste

* [3.1 Configuração do Ambiente](3-desenvolvimento-dirigido-por-teste/3-1-configuracao-do-ambiente.md)
* [3.2 Verificando o Ambiente com TDD](3-desenvolvimento-dirigido-por-teste/3-2-verificando-o-ambiente-com-tdd.md)
* [3.3 Controle de Versão do Projeto](3-desenvolvimento-dirigido-por-teste/3-3-controle-de-versao-do-projeto.md)
* [3.4 Teste Funcional com UnitTest](3-desenvolvimento-dirigido-por-teste/3-4-teste-funcional-com-unittest.md)
* [3.5 Teste de Unidade e a Evolução do Sistema](3-desenvolvimento-dirigido-por-teste/3-5-teste-de-unidade-e-a-evolucao-do-sistema/README.md)
  * [3.5.1 Teste de Unidade de uma View](3-desenvolvimento-dirigido-por-teste/3-5-teste-de-unidade-e-a-evolucao-do-sistema/3-5-1-teste-de-unidade-de-uma-view.md)
* [3.6 Evoluindo o Teste Funcional](3-desenvolvimento-dirigido-por-teste/3-6-evoluindo-o-teste-funcional.md)
* [3.7 Revisando o Processo do TDD](3-desenvolvimento-dirigido-por-teste/3-7-revisando-o-processo-do-tdd.md)

## 4 TDD E BANCO DE DADOS

* [4.1 Envio e Processamento de Requisição POST](4-tdd-e-banco-de-dados/4-1-envio-de-requisicao-post.md)
* [4.2 Banco de Dados no Django](4-tdd-e-banco-de-dados/4-2-banco-de-dados-no-django.md)

## 5 MELHORANDO E ORGANIZANDO OS CONJUNTOS DE TESTE

* [5.1 Isolamento dos Testes Funcionais](5-melhorando-e-organizando-os-conjuntos-de-teste/5-1-isolamento-do-testes-funcionais.md)
* [5.2 Esperas Implícitas, Explícitas e Time Sleeps](5-melhorando-e-organizando-os-conjuntos-de-teste/5-2-esperas-implicitas-explicitas-e-time-sleeps.md)

## 6 ATACANDO O MAIOR DESAFIO DE FORMA INCREMANTAL <a href="6-atacando-o-maior-desafio-de-forma-incremantal-1" id="6-atacando-o-maior-desafio-de-forma-incremantal-1"></a>

* [6.1 Separando URL no Estilo REST](6-atacando-o-maior-desafio-de-forma-incremantal-1/6-1-separando-url-no-estilo-rest.md)
* [6.2 Iterando para um Novo Design da Aplicação](6-atacando-o-maior-desafio-de-forma-incremantal-1/6.2-iterando-para-um-novo-design-da-aplicacao.md)
* [6.3 Refatorar Casos de Teste](6-atacando-o-maior-desafio-de-forma-incremantal-1/6-3-refatorar-casos-de-teste.md)
* [6.4 Separando Templates](6-atacando-o-maior-desafio-de-forma-incremantal-1/6-4-separando-templates.md)
* [6.5 URL para Nova Lista](6-atacando-o-maior-desafio-de-forma-incremantal-1/6-5-url-para-nova-lista.md)
* [6.6 Alterando Modelos](6-atacando-o-maior-desafio-de-forma-incremantal-1/6.6-alterando-modelos.md)
* [6.7 URL Próprio para Cada Lista](6-atacando-o-maior-desafio-de-forma-incremantal-1/6.7-url-proprio-para-cada-lista.md)
