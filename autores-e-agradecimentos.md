# Autores e Agradecimentos

Este livro tem e terá vários autores. Trata-se de um livro desenvolvido colaborativamente ao longo do tempo, profissionais responsáveis pela disciplina **ESBD2 - Programação e Desenvolvimento Dirigido por Testes**, ministrada no módulo do curso de [MBA Machine Learning in Production](https://iti.ufscar.mba/mlp) do [ITI-UFSCar](https://iti.ufscar.mba/).&#x20;

O [Prof. Auri Vincenzi](https://www.lapes.ufscar.br/members/professors/auri-marcelo-rizzo-vincenzi) (DC/UFSCar) é o idealizador e coordenador, entretanto, são vários os colaboradores que permitiram a escrita do conteúdo aqui disponibilizado. Desde já agradeço o empenho de todos no desenvolvimento deste material. Sem vocês certamente este projeto não poderia ser realizado.

**Contribuições na Edição de 2024**

* [Bruno Emílio Melo Barros](https://www.linkedin.com/in/brnemilio/) (Itaú) possui vasta experiência no uso do TDD como metodologia de desenvolvimento. Adicionalmente, contribuiu extensivamente para as práticas de engenharia do caos e testes voltados para acessibilidade digital, tema que também é objeto de sua pesquisa no curso de mestrado em ciências na USP e os relatos de experiência aqui apresentados.&#x20;

**Contribuições nas Edições de 2021 à 2023**

* [Pedro Sakuma Travi](https://www.linkedin.com/in/pedro-travi/) (Santander) possui vasta experiência no uso do TDD como metodologia de desenvolvimento e contribuiu extensivamente com as práticas de TDD e os relatos de experiência aqui apresentados.&#x20;
* Eduardo Molina (Raccoon) possui experiência em desenvolvimento Python e contribui com a melhoria, detalhamento das explicações de todo código Python presente neste material.&#x20;
* Jasiel Josias Lima Macagnan (PPGCC/UFSCar) mestrando do PPGCC/UFSCar, atua na área de desenvolvimento de aplicações computacionais, com experiência em linguagens de programação como Java, Python, Javascript, C++, entre outras tecnologias da área.
* A Profa. Marilde Terezinha Prado Santos (DC/UFSCar) é especialista em Educação a Distância, organizadora do MBA _Machine Learning in Production_ e contribui enormemente com o desenvolvimento do conteúdo aqui apresentado.

