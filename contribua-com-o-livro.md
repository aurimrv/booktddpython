# Contribua com o Livro

Aceitamos todo tipo de contribuição e apoio na redação e melhoria do conteúdo deste livro. Fique à vontade para mandar críticas, sugestões e melhorias via [Merge Request](https://gitlab.com/aurimrv/booktddpython/-/merge\_requests) no nosso repositório do [GitLab](https://gitlab.com/aurimrv/booktddpython).
