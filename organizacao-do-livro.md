# Organização do Livro

Este livro está organizado em 6 capítulos. O primeiro capítulo apresenta uma breve introdução aos conceitos de teste de software e desenvolvimento dirigido por teste.

No Capítulo 2 é apresentada uma revisão sobre teste de software, os principais conceitos, técnicas e critérios de teste, visando a contribuir para a geração de testes antes da implementação do produto de software.

No Capítulo 3 inicia-se com a  instalação das ferramentas básicas que será utilizadas no decorrer dos demais capítulos e, em seguida, apresenta-se o fluxo do TDD e os passos básicos de sua execução.

No Capítulo 4 é discutido sobre TDD e banco de dados e como o TDD assegura a correta execução das transações.

No Capítulo 5 são exploradas alternativas para melhor organizar os testes funcionais e tratamento de esperas explícidas visando a evitar a quebra dos testes por timeout.

No Capítulo 6 o sistema é evoluído com base no TDD e o processo de refatoração dos testes e da aplicação são executados.

...

...

...
